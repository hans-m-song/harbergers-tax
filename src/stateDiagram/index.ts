import {io} from './io';
import {Participant} from './participant';
import {State, StateMap, stringifyState} from './state';

const roster = [
  [0, 2],
  [1, 0],
  [1, 0],
];
const participants = roster.map(
  (pair, i) => new Participant(i, pair[0], pair[1]),
);
console.log(participants);

const root = new State(0, '0', participants).generateStates().visitStates();

const registry: StateMap = {};

io.output('```mermaid');
io.output('graph TD', 2);
io.output(stringifyState(root, registry), 4);
io.output('```');
