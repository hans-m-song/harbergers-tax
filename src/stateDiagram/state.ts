import {io} from './io';
import {Participant} from './participant';
import {
  PurchaseTransction,
  RewardTransaction,
  StateTransaction,
  TransactionType,
} from './stateTransaction';

const MAX_DEPTH = parseInt(process.env.MAX_DEPTH || '') || 5;
const DEBUG = process.env.DEBUG || false;

export type StateMap = {[stateId: string]: State};

export class State {
  depth: number;
  id: string;
  participants: Participant[];
  states: Map<StateTransaction[], State>;
  duplicatedStates: Map<StateTransaction[], string>;
  reward: boolean;

  constructor(
    depth: number,
    id: string,
    participants: Participant[],
    reward = false,
  ) {
    io.log('NEW', id, depth);
    this.depth = depth;
    this.id = id;
    this.participants = participants.map(
      (participant) =>
        new Participant(participant.id, participant.funds, participant.chunks),
    );
    this.states = new Map();
    this.duplicatedStates = new Map();
    this.reward = reward;
  }

  applyStateTransactions(transactions: StateTransaction[]) {
    transactions.forEach((transaction) => {
      switch (transaction.type) {
        case TransactionType.PURCHASE: {
          io.log(
            'APPLY',
            this.id,
            'purchase',
            transactions
              .map((transaction) => transaction.toString())
              .join(', '),
          );
          const {initiator, target} = transaction as PurchaseTransction;
          this.participants[initiator.id].funds -= 1;
          this.participants[initiator.id].chunks += 1;
          this.participants[target.id].funds += 1;
          this.participants[target.id].chunks -= 1;
          break;
        }
        case TransactionType.REWARD: {
          io.log('APPLY', this.id, 'rewards');
          this.participants.forEach(
            (participant) => (participant.funds += participant.chunks),
          );
          break;
        }
      }
    });
    return this;
  }

  generateStates() {
    if (this.depth > MAX_DEPTH) {
      io.log('GENERATE', this.id, 'max depth reached');
      return this;
    }
    const transactionSets =
      this.depth > 0 && this.depth % 2 !== 0
        ? [[new RewardTransaction()]]
        : generateActions(this.participants);

    const idList: string[] = [];
    transactionSets.forEach((transactionSet, i) => {
      const stateId = `${this.id}_${i}`;
      idList.push(stateId);
      const state = new State(
        this.depth + 1,
        stateId,
        this.participants,
      ).applyStateTransactions(transactionSet);

      const duplicate = duplicatedState(
        state,
        Array.from(this.states.values()),
      );
      if (!validState(state)) {
        return;
      }
      if (duplicate) {
        this.duplicatedStates.set(transactionSet, duplicate.id);
      } else {
        this.states.set(transactionSet, state);
      }
    });
    io.log('GENERATED', this.id, idList.join(', '));
    return this;
  }

  visitStates() {
    for (const state of this.states.values()) {
      io.log('VISIT', this.id, state.id);
      state.generateStates().visitStates();
    }
    return this;
  }

  semiEquals(other: State, allowCross = true): boolean {
    const [thisPool, thisFirst, thisSecond] = Object.values(this.participants);
    const [pool, first, second] = Object.values(other.participants);
    return (
      thisPool.semiEquals(pool) &&
      ((thisFirst.semiEquals(first) && thisSecond.semiEquals(second)) ||
        (allowCross &&
          thisFirst.semiEquals(second) &&
          thisSecond.semiEquals(first)))
    );
  }

  serialise(): object {
    const relations = [];
    for (const [transactionSet, state] of this.states) {
      relations.push({transactionSet, state: state.serialise()});
    }
    return {
      declaration: this.id,
      relations,
    };
  }

  toString(delim = ', ') {
    const debug = DEBUG ? `${this.depth}:${this.id}<br>` : '';
    return `${this.id}[${debug}${this.participants
      .map((participant) => participant.toString())
      .join(delim)}]`;
  }
}

const generateActions = (participants: Participant[]): StateTransaction[][] => {
  const transactions = Object.values(participants).reduce(
    (transactions, initiator) => {
      if (initiator.funds > 0) {
        Object.values(participants).forEach((target) => {
          if (target.id !== initiator.id && target.chunks > 0) {
            transactions.push(new PurchaseTransction(initiator, target));
          }
        });
      }
      return transactions;
    },
    [] as PurchaseTransction[],
  );
  const transactionSets: StateTransaction[][] = [];
  for (let i = 0; i < transactions.length - 1; i++) {
    const left = transactions[i];
    if (left instanceof PurchaseTransction && left.initiator.id === 0) {
      continue;
    }
    for (let j = i + 1; j < transactions.length; j++) {
      const right = transactions[j];
      if (
        left.equals(right) ||
        (right instanceof PurchaseTransction && right.initiator.id === 0) ||
        (left instanceof PurchaseTransction &&
          right instanceof PurchaseTransction &&
          left.initiator.equals(right.initiator))
      ) {
        continue;
      }
      transactionSets.push([left, right]);
    }
    transactionSets.push([left]);
  }
  transactionSets.push([transactions[transactions.length - 1]]);
  return transactionSets;
};

const validState = (state: State) =>
  Object.values(state.participants).reduce(
    (valid, participant) =>
      valid && participant.funds >= 0 && participant.chunks >= 0,
    true,
  );

const duplicatedState = (
  initialState: State,
  registry: State[],
): State | null => {
  const duplicate = registry.find((state) => initialState.semiEquals(state));
  return duplicate ? duplicate : null;
};

export const stringifyState = (
  state: State,
  registry: StateMap,
  duplicatedStateId?: string,
): string => {
  const stateId = duplicatedStateId || state.id;
  const declarations = new Set();
  if (!duplicatedStateId) {
    registry[state.id] = state;
    declarations.add(state.toString('<br>'));
  }
  const subDeclarations: string[] = [];
  for (const [transactionSet, subState] of state.states) {
    const transaction = transactionSet
      .map((transaction) => transaction.toString())
      .join('<br>');
    const duplicate = duplicatedState(subState, Object.values(registry));
    const debug = DEBUG ? `${state.depth}:${stateId}<br>` : '';
    declarations.add(
      `${stateId} --> |${debug}${transaction}| ${duplicate?.id || subState.id}`,
    );
    const subDeclaration = stringifyState(subState, registry, duplicate?.id);
    if (subDeclaration) subDeclarations.push(subDeclaration);
  }
  for (const [transactionSet, subStateId] of state.duplicatedStates) {
    if (transactionSet[0].type === TransactionType.REWARD) {
      continue;
    }
    const transaction = transactionSet
      .map((transaction) => transaction.toString())
      .join('<br>');
    const debug = DEBUG ? `${state.depth}:${stateId}<br>` : '';
    declarations.add(`${stateId} --> |${debug}${transaction}| ${subStateId}`);
  }
  return [...declarations, ...subDeclarations].join('\n');
};
